const router = require('express').Router();
const randomstring = require("randomstring");
const config = require('../../config/config');
const Storename = require('../../models/storename');

const passport=require('../../config/passport');



// router.get('/', (req, res) => {
	
			
// 			console.log(req.get('host'));
// 			const url=req.get('host');
// 			const store=url.split(".");
// 			const storeName=store[0];
// 			console.log("storename:"+store[0]);
// 			Storename.selectTable('store_details');
// 			Storename.getItem(storeName, {}, (err, name) => {
// 				console.log(name, err);
// 				if (err) {
// 					return res.send(statusMsg.errorResponse(err))
// 				} if (Object.keys(name).length === 0) {
// 					//res.send("storename not found");
// 					console.log("storename not found");
// 					// res.send("storename not found");
// 					res.render("err");
// 				}
// 				if (Object.keys(name).length > 0) {
// 					res.render("login");
// 				}
// 			});
		
// 			//res.send('This is the api home route for User management');
// 		})

router.get("/gl",function(req,res){
	   //res.render("login");
	  //res.send("please login");
	  res.redirect('/gl/auth/google');
})


router.get("/gl/logincheck",isLoggedIn,function(req,res){
	  var username=req.user.username;
	  console.log("username::"+username);
	  const storeName = req.user.storeName;
	  global.store = storeName;
	  //const queryParams =JSON.stringify(req.query);
	  res.redirect("https://"+storeName+".cadenza.tech/?username="+username);
	  
})

router.get('/gl/auth/storeName', (req, res) => {
	if(req.query.storename){
		const storeName = req.query.storename;
		req.session.storename = storeName;
		
	res.redirect("https://"+storeName+".cadenza.tech/gl/auth/google");
	}
})

//Login
router.get('/gl/auth/google',passport.authenticate('google', { scope: ['openid email profile'] }));

// Authentication check	
router.get('/gl/auth/google/callback',passport.authenticate('google', {
                    successRedirect : '/gl/logincheck',
                    failureRedirect : '/gl'
}));

// Logout
router.get('/gl/logout', function(req, res) {
	req.logout();
	res.redirect('/gl/logincheck');
});

function isLoggedIn(req, res, next) {
	if (req.isAuthenticated()) {
	  return next();
	}
	//res.send("login");
	res.redirect('/gl');
  }

module.exports = router;

