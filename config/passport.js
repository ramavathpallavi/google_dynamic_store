const  authConfig = require('./auth');
const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
//dynamodb connection
const User = require('../models/user');
const bcrypt = require('bcryptjs');
const encrypt = require('../methods/encrypt');
const statusMsg = require('../methods/statusMsg');
const config = require('../config/config');
const Methods = require('../methods/custom');

 global.store="";
passport.serializeUser(function(user, done) {
    // done(null, user.id);
    console.log("serialize: "+JSON.stringify(user));
    done(null, user);
  });
  
  passport.deserializeUser(function(obj, done) {
    // Users.findById(obj, done);
    console.log("deserialize: "+JSON.stringify(obj));
    done(null, obj);
  });

  passport.use(new GoogleStrategy({
		
    clientID        : authConfig.google.clientID,
    clientSecret    : authConfig.google.clientSecret,
 //   callbackURL     : authConfig.google.callbackURL,
    callbackURL     : "https://"+store+".cadenza.tech/g/auth/google/callback",
    passReqToCallback: true
  },function(req, accessToken, refreshToken,profile, done) {
       var json=JSON.stringify(profile);
       console.log("profile: "+json);
       var profile=JSON.parse(json);
      // const storename = req.session.storename || "empty";
      //console.log('\npasport req.session:\n', JSON.stringify(req.session));
      const storename = req.session.storename;
      console.log("storename => "+storename);

              process.nextTick(function() {
                const userEmail=profile.emails[0].value;
                const tableName= storename+"_user_data";
                User.selectTable(tableName);
                User.getItem(userEmail, {}, (err, user) => {
                  console.log(user, err);
                  if (err) {
                    //return res.send(statusMsg.errorResponse(err))
                    return done(err);
                  } if (Object.keys(user).length === 0) {
                    console.log('user',user);
                    //res.send(user)
                      const putParams = {
                        "email": userEmail,
                        "username": profile.name.givenName,
                        
                      };
                      console.log("Adding a new item...\n", putParams);
                      //User.selectTable(tableName);
                      User.createItem(putParams, { overwrite: false,tableName:tableName }, (err, user) => {
                        if (err) {
                          res.send(statusMsg.errorResponse(err));
                        } else {
                          console.log('\nAdded\n', user);
                          putParams['storeName'] = storename;
                          return done(null, putParams);  
                        }
                      });
                    
                    
                    
                  }
                  if (Object.keys(user).length > 0) {
                    user['storeName'] = storename;
                    return done(null, user);
                  }
            })

               

            });
    }
));  


module.exports = passport;
