module.exports = {

    'facebook' : {
        'clientID'      : 'your-secret-clientID-here', // your App ID
        'clientSecret'  : 'your-client-secret-here', // your App Secret
        'callbackURL'   : 'http://localhost:8080/auth/facebook/callback'
    },

    'twitter' : {
        'consumerKey'       : 'your-consumer-key-here',
        'consumerSecret'    : 'your-client-secret-here',
        'callbackURL'       : 'http://localhost:8080/auth/twitter/callback'
    },

    'google' : {
        'clientID': '928679407685-rgperdfi4d0qqb3dt78dhvgo6qfc1f22.apps.googleusercontent.com',
        'clientSecret': 'sS5BIZOVh5AuwFfvDs-pFkan',
        'callbackURL': 'https://google.cadenza.tech/gl/auth/google/callback'
    }

};
